import { Player, Game } from './game/game.interface';

const darth: Player = {
    id: "DarthxVaderXD",
    name: "DarthxVaderXD",
    chips: 1000,
    avatar: "hi",
};

const ian: Player = {
    id: "thatoneguy",
    name: "thatoneguy",
    chips: 1000,
    avatar: "hi",
};

const katie: Player = {
    id: "tiggerlover99",
    name: "tiggerlover99",
    chips: 1000,
    avatar: "hi",
};

const josiah: Player = {
    id: "jojocoolswagboss",
    name: "jojocoolswagboss",
    chips: 1000,
    avatar: "hi",
};

const rj: Player = {
    id: "rj",
    name: "rj",
    chips: 1000,
    avatar: "hi",
};

const logie: Player = {
    id: "logos",
    name: "logos",
    chips: 1000,
    avatar: "hi",
};

const broke: Player = {
    id: "broke",
    name: "broke",
    chips: 0,
    avatar: "hi",
};

const game = new Game(0, 9, 100);

console.log(`New Game ${game.getGameId()}`);

game.addPlayer(darth);
game.addPlayer(ian);
game.addPlayer(katie);
game.addPlayer(josiah);
game.addPlayer(rj);
game.addPlayer(logie);
game.addPlayer(broke);

const hands = [
    {
        blinds:[
            {
                player: darth,
                bet: 100,
            },
            {
                player: ian,
                bet: 100,
            },
            {
                player: katie,
                bet: 0,
            },
            {
                player: josiah,
                bet: 0,
            },
            {
                player: rj,
                bet: 100,
            },
            {
                player: logie,
                bet: 0,
            },
        ],
        flop: [
            {
                player: darth,
                bet: 100,
            },
            {
                player: ian,
                bet: 100,
            },
            {
                player: rj,
                bet: 0,
            },
        ],
        turn: [
            {
                player: darth,
                bet: 100,
            },
            {
                player: ian,
                bet: 100,
            },
        ],
        river: [
            {
                player: darth,
                bet: 100,
            },
            {
                player: ian,
                bet: 100,
            },
        ]
    },
    {
        blinds:[
            {
                player: darth,
                bet: 9999,
            },
            {
                player: ian,
                bet: 9999,
            },
            {
                player: katie,
                bet: 9999,
            },
            {
                player: josiah,
                bet: 9999,
            },
            {
                player: rj,
                bet: 0,
            },
            {
                player: logie,
                bet: 0,
            },
        ],
    },
];

function doBet(bet: any) {
    let amount = 0;
    if (bet.bet === 9999) {
        amount = bet.player.chips;
        game.bet(bet.player, bet.player.chips);
    } else if (bet.bet === 0) {
        game.fold(bet.player);
    } else {
        amount = bet.bet;
        game.bet(bet.player, bet.bet);
    }

    if (amount > 0) {
        console.log(`\t\t${bet.player.name} bets ${amount}`);
        console.log(`\t\t\t ${bet.player.name} has ${bet.player.chips} left`);
    } else {
        console.log(`\t\t${bet.player.name} folds`);
        console.log(`\t\t\t ${bet.player.name} has ${bet.player.chips} left`);
    }
}

if (game.start()) {
    console.log("Game Started");
    hands.forEach(hand => {
        if (game.deal()) {
            const dealer = game.getDealer();
            const smallBlind = game.getSmallBlind();
            const bigBlind = game.getBigBlind();
            console.log('New hand');
            console.log(`\tDealer => ${dealer.name}\t sb => ${smallBlind.name}\t bb => ${bigBlind.name}`);

            // blinds
            if (typeof hand.blinds != 'undefined') {
                hand.blinds.forEach(bet => {
                    doBet(bet);
                });
            }

            game.closeBetting();
            
            if (game.getState() === 3) {
                // flop
                console.log('\tThe flop => ', game.getFlop());
                
                if (typeof hand.flop !== 'undefined') {
                    hand.flop.forEach(bet => {
                        doBet(bet);
                    });
                }

                game.closeBetting();

                // turn
                if (game.getState() === 4) {
                    console.log('\tThe turn => ', game.getTurn());

                    if (typeof hand.turn !== 'undefined') {
                        hand.turn.forEach(bet => {
                            doBet(bet);
                        });
                    }
    
                    game.closeBetting();
                }

                // river
                if (game.getState() === 5) {
                    console.log('\tThe turn => ', game.getRiver());

                    if (typeof hand.river !== 'undefined') {
                        hand.river.forEach(bet => {
                            doBet(bet);
                        });
                    }
    
                    game.closeBetting();
                }
            }

            if (game.getState() === 6) {
                console.log("\tand the winners are....");
                const results = game.showDown();
                Object.keys(results).forEach(result => {
                    game.processWinners(results[result].winners);

                    results[result].winners.forEach(winner => {
                        console.log(`\tWinner: ${winner.player.name} won ${winner.amount} with ${winner.desc}`
                            + ` they now have ${winner.player.chips} in chips`
                        );
                        console.log(winner);
                    });
                });

                if (game.shuffle()) {
                    console.log("Shuffling the deck");
                }
            }
        } else {
            console.log("Cannot deal");
        }
    }); 
} else {
    console.log("Cannot start game");
}