import { Identifier } from "@babel/types";
import { Pot, PotResults } from "./classes/pot";
import { Deck } from "./classes/deck";
import { Hand } from "./classes/hand";
import { v4 } from "uuid";
import {Hand as pokersolver} from "pokersolver";

// const pokersolver = require('pokersolver').Hand;

export enum GameState {
    'waiting', // 0
    'shuffeling', // 1
    'blinds', // 2
    'flop', // 3
    'turn', // 4
    'river', // 5
    'showdown', // 6
    'break', // 7
    'closed', // 8
}

export enum GameType {
    'Hold-em',
    'Pinapple',
    'Omaha',
    'Razz'
}

export interface Player {
    id: string,
    name: string;
    chips: number;
    avatar: string;
}

export class Game {
    private id: string;
    private type: number;
    private state: number;
    private hand: Hand;
    private players: Array<Player> = [];
    private pots: Array<Pot> = [];
    private deck: Deck;
    private maxPlayers: number;
    private dealer: number = -1;
    private blind: number = 100;
    private folded: Array<Player>;

    public constructor(type: number, maxPlayers: number, blind: number = 100) {
        this.id = v4();
        this.type = type;
        this.state = 0;
        this.maxPlayers = maxPlayers;
        this.blind = blind;
        this.deck = new Deck();
    }

    public getGameId(): string {
        return this.id;
    }
    
    public getType(): number {
        return this.type;
    }

    public getState(): number {
        return this.state;
    }

    public getHand(): Hand {
        return this.hand;
    }

    public getPlayers(): Array<Player> {
        return this.players;
    }

    public findPlayer(id: string) {
        return this.players.find(p => p.id === id);
    }

    public addPlayer(player: Player): boolean {
        if (this.players.length < this.maxPlayers && !this.findPlayer(player.id)) {
            this.players.push(player);
            return true;
        }

        return false;
    }

    public removePlayer(player: Player): boolean {
        const index = this.players.findIndex(p => p.id === player.id);
        
        if (index > -1) {
            this.players.splice(index, 1);
            return true;
        }

        return false;
    }

    public start(): boolean {
        if (this.state === 0) {
            this.state = 1;
            
            if (this.dealer == -1) {
                this.dealer = Math.floor(Math.random() * this.players.length);
            }

            return true;
        }

        return false;
    }

    public shuffle(): boolean {
        if (this.state > 0 && this.state < 7) {
            this.state = 1;
            return true;
        }

        return false;
    }

    public deal(): boolean {
        if (this.state === 1) {
            this.state = 2;
            
            this.pots = [];
            this.folded = [];

            const players: Array<Player> = [];
            let current = this.dealer;
            for (let i = 0; i < this.players.length; i += 1) {
                players.push(this.players[current]);

                if (this.players[current].chips < 1) {
                    this.folded.push(this.players[current]);
                }

                current += 1;
                if (current >= this.players.length) {
                    current = 0;
                }
            }

            this.deck.shuffle(false);
            this.hand = new Hand(players, this.deck);

            this.setupBlinds();

            return true;
        }

        return false;
    }

    public getPots(): Array<Pot> {
        return this.pots;
    }

    public bet(player: Player, amount: number): boolean {
        if (this.state >= 2 && this.state <= 5 && player.chips > 0) {
            const index = this.players.findIndex(p => p.id === player.id);
            const foldedIndex = this.folded.findIndex(p => p.id === player.id);
            if (index > -1 && foldedIndex === -1) {
                if (this.players[index].chips >= amount) {
                    let type = 3; // bet
                    if (amount < this.pots[this.pots.length - 1].getCurrentBet() || amount == this.players[index].chips) {
                        type = 7; // all in
                    } else if (amount === this.pots[this.pots.length - 1].getCurrentBet()) {
                        type = 4; // call
                    } else if (amount >= this.pots[this.pots.length - 1].getCurrentBet() && this.pots[this.pots.length - 1].getCurrentBet() > 0) {
                        type = 5; // raise
                    }

                    this.players[index].chips = this.players[index].chips - amount;
                    this.pots[this.pots.length - 1].bet(this.players[index], amount, type);
                    return true;
                }
            }
        }

        return false;
    }

    public fold(player: Player): boolean {
        if (this.state >= 2 && this.state <= 5) {
            const index = this.players.findIndex(p => p.id === player.id);
            if (index > -1) {
                this.pots[this.pots.length - 1].fold(this.players[index]);
                this.folded.push(this.players[index]);
                return true;
            }

            if (this.folded.length === this.players.length - 1) {
                this.state = 6;
            }
        }

        return false;
    }

    public closeBetting(): boolean {
        if (this.state >= 2 && this.state <= 5) {
            const players = [];

            for (let i = 0; i < this.players.length; i += 1) {
                const index = this.folded.findIndex(p => p.id === this.players[i].id);
                if (index > -1) {
                    if (this.players[i].chips > 0) {
                        players.push(this.players[i]);
                    }
                }
            }

            /**
             * if (this.state < 5) {
             *   const pot = new Pot(players);
             *   this.pots.push(pot);
             * }
             */

            this.state += 1;

            return true;
        }

        return false;
    }

    public getDealer(): Player {
        return this.players[this.dealer];
    }

    public getSmallBlind(): Player {
        let sb = this.dealer + 1;
        if (sb === this.players.length) {
            sb = 0;
        }

        return this.players[sb];
    }

    public getBigBlind(): Player {
        let bb = this.dealer + 1;
        if (bb === this.players.length) {
            bb -= this.players.length;
        }

        return this.players[bb];
    }

    public getFlop(): Array<string> {
        return this.hand.getFlop();
    }

    public getTurn(): string {
        return this.hand.getTurn();
    }

    public getRiver(): string {
        return this.hand.getRiver();
    }

    public showDown(): any {  // TODO: figure out how to make this a defined object
        const hands = this.getHandData();
        const handKeys = Object.keys(hands);
        const winners = [];

        handKeys.forEach((handKey) => {
            winners.push(pokersolver.winners(hands[handKey].hands));
        });

        for (let i = 0; i < winners.length; i += 1) {
            for (let k = 0; k < winners[i].length; k += 1) {
                const winnerId = hands[handKeys[i]].hands.findIndex(h => h === winners[i][k]);
                if (winnerId > -1) {
                    if (typeof hands[handKeys[i]].winners == 'undefined') {
                        hands[handKeys[i]].winners = [];
                    }
                    
                    hands[handKeys[i]].winners.push({
                        player: hands[handKeys[i]].players[winnerId],
                        hand: hands[handKeys[i]].hands[winnerId].cards,
                        desc: winners[i][k].descr,
                        amount: hands[handKeys[i]].amount * hands[handKeys[i]].players.length,
                    });
                }
            }
        }
        
        return hands;
    }

    public processWinners(results: Array<PotResults>): void {
        const winnings = Math.ceil(results[0].amount / results.length);
        results.forEach((result: PotResults) => {
            const player = this.players.find(p => p === result.player);
            player.chips += winnings;
        });
    }

    public neededToCall(player: Player): number {
        // TODO: this is a stub and I need to figure out how to do this        
        return 0;
    }

    private setupBlinds(): void {
        const pot: Pot = new Pot(this.players);
        let sb: number = this.dealer + 1;
        let bb: number = this.dealer + 2;

        if (sb >= this.players.length) {
            sb = sb - this.players.length;
        }

        if (bb >= this.players.length) {
            bb = bb - this.players.length;
        }

        let sbAmount = Math.ceil(this.blind / 2);
        let bbAmount = Math.ceil(this.blind);

        if (this.players[sb].chips < sbAmount) {
            sbAmount = this.players[sb].chips;
        }

        if (this.players[bb].chips < bbAmount) {
            bbAmount = this.players[bb].chips;
        }

        this.players[sb].chips -= sbAmount;
        this.players[bb].chips -= bbAmount;

        pot.bet(this.players[sb], sbAmount, 0);
        pot.bet(this.players[bb], bbAmount, 1);

        this.pots.push(pot);
    }

    private getHandData(): any {
        const bets = this.pots[0].getPlayerBets();
        const hands = {};
        let total = 0;

        for (let i = 0; i < bets.length; i += 1) {
            if (bets[i].amount === 0) { continue; }
            const index = this.folded.findIndex(p => p.id === bets[i].player.id);

            if (index === -1) {
                const keys = Object.keys(hands);
                const hand = pokersolver.solve([
                    ...this.hand.getPlayerHand(bets[i].player).hand,
                    ...this.hand.getFlop(),
                    this.hand.getTurn(),
                    this.hand.getRiver(),
                ]);

                keys.forEach((key) => {
                    if (Number(key) !== bets[i].amount) {
                        hands[key].players.push(bets[i].player);
                        hands[key].hands.push(hand);
                    }
                });

                if (typeof hands[bets[i].amount] === 'undefined') {
                    hands[bets[i].amount] = {
                        players: [bets[i].player],
                        hands: [hand],
                        amount: bets[i].amount - total,
                    }

                    total = bets[i].amount;
                } else {
                    hands[bets[i].amount].players.push(bets[i].player);
                    hands[bets[i].amount].hands.push(hand);
                }
            }
        }

        return hands;
    }
}