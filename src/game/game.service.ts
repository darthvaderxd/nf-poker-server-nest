import { Injectable } from '@nestjs/common';
import { Game } from './game.interface';

@Injectable()
export class GameService {
    private readonly games: Array<Game> = [];

    public getGames(): Array<Game> {
        return this.games;
    }

    public newGame(game: Game): void {
        this.games.push(game);
    }

    public findGame(id: string): Game {
        return this.games.find(game => game.getGameId() === id);
    }
}
