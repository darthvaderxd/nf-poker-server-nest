import { Player } from "../game.interface";
import { Deck } from "./deck";

export interface PlayerHand {
    playerId: string;
    hand: Array<String>;
}

export class Hand {
    private players: Array<Player> = [];
    private hands: Array<PlayerHand> = [];
    private deck: Deck;
    private burn: Array<string> = [];
    private flop: Array<string> = [];
    private turn: string;
    private river: string;

    // currently only supports texas holdem
    public constructor(players: Array<Player>, deck: Deck) {
        this.players = players;
        this.deck = deck;

        // need to loop twice
        for (let i = 0; i < this.players.length; i += 1) {
            const player: Player = this.players[i];
            if (player.chips > 0) {
                this.hands.push({
                    playerId: player.id,
                    hand: [this.deck.nextCard()],
                });
            } else {
                this.hands.push({
                    playerId: player.id,
                    hand: [],
                });
            }
        }

        for (let i = 0; i < this.players.length; i += 1) {
            const player: Player = this.players[i];
            if (player.chips > 0) {
                const hand: PlayerHand = this.hands.find(h => h.playerId === player.id);
                hand.hand.push(this.deck.nextCard());
            }
        }

        this.burn.push(this.deck.nextCard());
        this.flop.push(...[this.deck.nextCard(), this.deck.nextCard(), this.deck.nextCard()]);
        this.burn.push(this.deck.nextCard());
        this.turn = this.deck.nextCard();
        this.burn.push(this.deck.nextCard());
        this.river = this.deck.nextCard();
    }

    public getPlayerHands(): Array<PlayerHand> {
        return this.hands;
    }

    public getPlayerHand(player: Player): PlayerHand {
        return this.hands.find(h => h.playerId === player.id);
    }

    public getFlop(): Array<string> {
        return this.flop;
    }

    public getTurn(): string {
        return this.turn;
    }

    public getRiver(): string {
        return this.river;
    }

    public getBurn(): Array<string> {
        return this.burn;
    }
}