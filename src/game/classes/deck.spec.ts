import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { Deck } from "./deck";

const deck = new Deck();

describe("Deck Class", () => {
    it("getDeck returns as expected with no wilds", () => {
        const emptyCards = deck.getDeck();
        expect(emptyCards.length).toBe(0);
        
        deck.shuffle();
        const cards = deck.getDeck();
        expect(cards.length).toBe(52);
        expect(deck.isWild()).toBe(false);

        deck.shuffle();
        const otherCards = deck.getDeck();
        expect(cards).not.toEqual(otherCards);
        expect(otherCards.length).toBe(52);
        expect(deck.isWild()).toBe(false);

        const uniqueCards = [];
        for(let i = 0; i < cards.length; i += 1) {
            const index = otherCards.findIndex(c => c === cards[i]);

            expect(index).not.toBe(-1);

            if (uniqueCards.findIndex(c => c === cards[i]) === -1) {
                uniqueCards.push(cards[i]);
            }
        }

        expect(uniqueCards.length).toBe(52);
    });

    it("getDeck returns as expected with no wilds", () => {
        deck.shuffle(true);
        const cards = deck.getDeck();

        expect(cards.length).toBe(52 + 4);
        expect(deck.isWild()).toBe(true);
    });

    it("nextCard works as expected", () => {
        deck.shuffle();
        const cards = deck.getDeck();

        expect(deck.isWild()).toBe(false);
        
        for (let i = 0; i < 52; i += 1) {
            expect(deck.getCardsLeft()).toBe(52 - i);
            const card = deck.nextCard();
            expect(typeof card).toBe('string');
            expect(cards[i]).toEqual(card);
        }

        expect(() => { deck.nextCard() }).toThrowError("There are no more cards");
    });
});