import { Player } from "../game.interface";

export enum PotAction {
    'Small Blind', // 0
    'Big Blind', // 1
    'Check', // 2
    'Bet', // 3
    'Call', // 4
    'Raise', // 5
    'Fold', // 6
    'All In', // 7
}

export interface PotHistory {
    player: Player;
    amount: number;
    type: string;
}

export interface PotResults {
    player: Player;
    hand: Array<any>;
    desc: string;
    amount: number;
}

export class Pot {
    protected total: number = 0;
    protected players: Array<Player> = [];
    protected history: Array<PotHistory> = [];
    protected maxBet: number = -1;
    protected currentBet: number = 0;

    public constructor(players: Array<Player>) {
        this.players = players;
    }

    public getTotal(): number {
        return this.total;
    }

    public getPlayers(): Array<Player> {
        return this.players;
    }

    public getHistory(): Array<PotHistory> {
        return this.history;
    }

    public getMaxBet(): number {
        return this.maxBet;
    }

    public getCurrentBet(): number {
        return this.currentBet;
    }

    public bet(player: Player, amount: number, type: number): void {
        if (this.currentBet < amount) {
            this.currentBet = amount;
        }

        this.history.push({
            player,
            amount,
            type: PotAction[type], 
        });

        this.total += amount;
    }

    public fold(player: Player): void {
        this.history.push({
            player,
            amount: 0,
            type: PotAction[6],
        });
    }

    public getPlayerBets(): Array<PotHistory> {
        const bets = [];

        for (let i = 0; i < this.history.length; i += 1) {
            const player = this.history[i].player;
            const index = bets.findIndex(h => h.player.id === player.id);

            if (index > -1) {
                bets[index].amount += this.history[i].amount;
            } else {
                bets.push(this.history[i]);
            }
        }

        bets.sort((a: PotHistory, b: PotHistory) => {
            return a.amount - b.amount;
        });

        return bets;
    }
}