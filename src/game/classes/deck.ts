const playingCards: Array<string> = [
    'Ac', 'Kc', 'Qc', 'Jc', 'Tc', '9c', '8c', '7c', '6c', '5c', '4c', '3c', '2c',
    'Ad', 'Kd', 'Qd', 'Jd', 'Td', '9d', '8d', '7d', '6d', '5d', '4d', '3d', '2d',
    'As', 'Ks', 'Qs', 'Js', 'Ts', '9s', '8s', '7s', '6s', '5s', '4s', '3s', '2s',
    'Ah', 'Kh', 'Qh', 'Jh', 'Th', '9h', '8h', '7h', '6h', '5h', '4h', '3h', '2h',
];

export class Deck {
    private deck: Array<string> = [];
    private useWild: boolean = false;
    private currentCard: number = -1;

    public getDeck(): Array<string> {
        return this.deck;
    }

    public isWild(): boolean {
        return this.useWild === true;
    }

    public getCardsLeft(): number {
        return this.deck.length - this.currentCard - 1;
    }

    public shuffle(useWild = false): void {
        this.deck = [...playingCards];
        this.useWild = useWild;
        this.currentCard = -1;

        if (useWild) {
            const wild = ['0c', '0h', '0d', '0s'];
            this.deck.push(...wild);
        }

        for (let i = this.deck.length - 1; i > 0; i -= 1) {
            const j = Math.floor(Math.random() * (i + 1));
            [this.deck[i], this.deck[j]] = [this.deck[j], this.deck[i]];
        }
    }

    public nextCard(): string {
        this.currentCard += 1;
        if (this.currentCard >= this.deck.length) {
            throw new Error("There are no more cards");
        }
        return this.deck[this.currentCard];
    }
}